<?php

namespace App\Http\Controllers;

use App\Models\directorsModel;
use Illuminate\Http\Request;
use App\Models\movieModel;


class MovieController extends Controller
{
    protected $movieModel;



    public function __construct()
    {
        $this->movieModel=new movieModel;
    }

    public function index(){

        return view('movies.index');

    }

    public function listApi(){

        return response()->json($this->movieModel->paginate(20));

    }
    public function getMovie($movieId){

        return view('movies.movie',$this->movieModel->getMovie($movieId));

    }



}