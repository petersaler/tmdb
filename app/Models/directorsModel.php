<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class directorsModel extends Model
{
    use HasFactory;

    protected $table = 'directors';

    protected $fillable = [
        'name',
        'tmdb_id',
        'biography',
        'date_of_birth'
    ];

    public $timestamps =false;

}
