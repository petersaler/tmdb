<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class movieModel extends Model
{
    use HasFactory;

    protected $table = 'movies';

    protected $fillable = [
        'title',
        'overview',
        'poster_url',
        'tmdb_id',
        'tmdb_vote_average',
        'tmdb_vote_count',
        'tmdb_url',
        'release_date',
        'updated_at',
    ];

    public function genres()
    {

        return $this->belongsToMany(
            genresModel::class,
            'movies_genres',
            'movie_id',
            'genre_id');
    }

    public function directors()
    {
        return $this->belongsToMany(
            directorsModel::class,
            'movies_directors',
            'movie_id',
            'director_id');
    }

    public function getSummFreshMovies(){
        $summ=movieModel::select('id')->where('updated_at','>', Carbon::today())->count();
        return $summ;

    }

    public function deleteOutdatedMovies(){
        $movies=movieModel::select('id')->where('updated_at','<', Carbon::today())->get();
        foreach ($movies as $movie){
        $movie->genres()->detach();
        $movie->directors()->detach();
        $movie->delete();
        }

    }
    public function getMovie($movieId){
        return movieModel::where('id',$movieId)->with('genres')->with('directors')->orderBy('tmdb_id', 'DESC')->firstOrFail();
    }

}
