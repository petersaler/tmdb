<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SallerPeter\Tmdbapi\Tmdb;
use App\Models\movieModel;
use App\Models\genresModel;
use App\Models\directorsModel;


class getTopRatedMoviesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $tmdb;
    protected $movieModel;
    protected $number;
    protected $exp1;
    protected $exp2;

    public function __construct()
    {
        $this->tmdb=new Tmdb;
        $this->movieModel=new movieModel;

        $number= $this->movieModel->getSummFreshMovies();

        $this->number=$number/20;

        switch ($this->number){
            case 0:
                $this->exp1=1;
                $this->exp2=3;
                break;
            case 10:
                $this->exp1=11;
                $this->exp2=11;
                break;
            case 10.5:
              break;
            default:
                if(is_int($this->number)){
                    $this->exp1=$this->number+1;
                }else{
                    $this->exp1=round($this->number);
                }
                $this->exp2=$this->exp1 +2;
        }
var_dump($this->number);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->exp1){
            $moviesArray=array();

            for ($x = $this->exp1; $x <= $this->exp2; $x++) {
                $this->tmdb->getTopRatedMovies($x);
                $array= $this->tmdb->fetch();
                $moviesArray = array_merge_recursive($moviesArray,$array);
            }

            $moviesArray=self::spliceTheArray($moviesArray['results']);

            foreach ($moviesArray as $movie){
                $movieDetails=self::getMovieDetails($movie['id']);
                $movieDirectors=self::getdirectors($movie['id']);

                $movieModel= $this->movieModel::firstOrNew(['tmdb_id' => $movie['id']]);
                $movieModel->title=$movie['title'];
                $movieModel->overview=$movie['overview'];
                $movieModel->poster_url=$movie['poster_path'];
                $movieModel->tmdb_id=$movie['id'];
                $movieModel->tmdb_vote_average=$movie['vote_average'];
                $movieModel->tmdb_vote_count=$movie['vote_count'];
                $movieModel->tmdb_url=$movie['id'].'-'. str_replace(' ', '-', $movie['title']);
                $movieModel->release_date=$movie['release_date'];
                $movieModel->length=$movieDetails['runtime'];
                $movieModel->updated_at=date('Y-m-d H:i:s ');
                $movieModel->save();

                foreach ($movieDetails['genres'] as $genre){
                    $genreModel = genresModel::firstOrNew(['id' => $genre['id']]);
                    $genreModel->id=$genre['id'];
                    $genreModel->name=$genre['name'];
                    $genreModel->save();
                    $movieModel->genres()->sync($genreModel);
                }
                foreach ($movieDirectors as $movieDirector){
                    $directorModel=directorsModel::firstOrNew(['tmdb_id' => $movieDirector['id']]);
                    $directorModel->name=$movieDirector['name'];
                    $directorModel->tmdb_id=$movieDirector['id'];
                    $directorModel->biography=$movieDirector['biography'];
                    $directorModel->date_of_birth=$movieDirector['birthday'];
                    $directorModel->save();
                    $movieModel->directors()->sync($directorModel);
                }

            }

            if ($this->number==10){
                $this->movieModel->deleteOutdatedMovies();
            }
        }else{
        return;
        }
    }

    public function getMovieDetails($movieId){
        $this->tmdb->getMovieDetails($movieId);
        return $this->tmdb->fetch();

    }

    public function getdirectors($movieId){
        $this->tmdb->getMovieCredits($movieId);
        $Directors=array();
        $movieCredits=$this->tmdb->fetch();
        foreach ($movieCredits['crew'] as $crew){
            if ($crew['job']=="Director"){
                $Director=self::getPerson($crew['id']);
                $Directors[]=$Director;
            }
        }
        return $Directors;
    }
    public function getPerson($personId){
        $this->tmdb->getPerson($personId);
        return $this->tmdb->fetch();

    }


    protected function spliceTheArray($array,$length=50){

        if (is_int($this->number)){
            $offset=0;
        }else{
            $offset=10;
        }
        if ($this->number==10){
            $length=10;
        }

        $array=array_splice($array, $offset, $length);
        return $array;
    }

}
