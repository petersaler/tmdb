<?php


namespace SallerPeter\Tmdbapi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Tmdb
{
    protected  $apirooturl;
    protected  $url;


    function __construct() {

        $this->apirooturl = "https://api.themoviedb.org/3/";


    }

    public  function getTopRatedMovies($page=1){

        $this->url= $this->apirooturl ."movie/top_rated?api_key=".config('tmdb.key')."&page=".$page;


    }

    public  function getGenres(){

        $this->url= $this->apirooturl."genre/movie/list?api_key=".config('tmdb.key');

    }

    public  function getMovieCredits($movieId){

        $this->url= $this->apirooturl."movie/".$movieId."/credits?api_key=".config('tmdb.key');

    }

    public function getPerson($personId){

        $this->url= $this->apirooturl."person/".$personId."?api_key=".config('tmdb.key');

    }

    public function getMovieDetails($movieId){

        $this->url= $this->apirooturl."movie/".$movieId."?api_key=".config('tmdb.key');

    }

    public function fetch(){
        $client= new client([
            'base_uri'=>$this->url
        ]);

        try{
            $res= $client->request('GET');

            $genres=json_decode($res->getBody()->getContents(), true);

            return $genres;

        }catch (GuzzleException $e){

            return response()->json([
                'error' => $e->getMessage()
            ]);
        }

    }

}