<?php

namespace SallerPeter\Tmdbapi;

use Illuminate\Support\ServiceProvider;
use SallerPeter\Tmdbapi\Tmdb;


class TmdbServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/tmdb.php' => config_path('tmdb.php'),
        ]);

    }

    public function register()
    {
    $this->app->singleton(Tmdb::class, function() {
       return new Tmdb();
    });
    }
}