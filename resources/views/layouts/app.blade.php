<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="bg-gray-800">
                <a class="text-white px-3 py-2 rounded-md text-medium font-medium" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>

    </nav>
    @yield('content')
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

