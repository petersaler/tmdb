@extends('layouts.app')
@section('content')

    <div class="container">

        <!-- Portfolio Item Heading -->
        <h1 class="my-4">{{$title}}
        </h1>

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-8">
                <img class="img-fluid" src="https://image.tmdb.org/t/p/w300_and_h450_bestv2/{{$poster_url}}" alt="{{$title}}">



            </div>

            <div class="col-md-4">
                <h3 class="my-3">Movie Description</h3>
                <p>{{$overview}}</p>
                <h3 class="my-3">Details</h3>
                <table class="table">
                    <tbody>
                    <tr>
                        <td>release date </td>
                        <td>{{$release_date}}</td>

                    </tr>
                    <tr>
                        <td>tmdb id</td>
                        <td>{{$tmdb_id}}</td>
                    </tr>
                    <tr>
                        <td>tmdb vote average</td>
                        <td>{{$tmdb_vote_average}}</td>
                    </tr>
                    <tr>
                        <td>tmbd vote count </td>
                        <td>{{$tmdb_vote_count}}</td>
                    </tr>
                    <tr>
                        <td>length</td>
                        <td>{{$length}} min</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <a target="_blank" class="btn btn-primary" href="https://www.themoviedb.org/movie/{{$tmdb_url}}" role="button">Check on TMDB</a><br>

        </div>
        <!-- /.row -->
        <a  class="btn btn-secondary" href="/" role="button">back</a>
        <!-- Related Projects Row -->
        <h3 class="text-center">Genres:</h3>


        @foreach ($genres as $genre)  <h4 class="text-center">{{ $genre['name']}}</h4> @endforeach
        <h3 class="text-center">Directors:</h3>
        <div class="row pt-4">


            @foreach ($directors as $director)
            <div class="col-md-4 col-sm-12">
                <h4>{{ $director['name']}}</h4>
                <h5>DOB: {{ $director['date_of_birth']}}</h5>
                <h5>TMDB ID: {{ $director['tmdb_id']}}</h5>
                <p>{{ $director['biography']}}</p>

            </div>
            @endforeach


        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

@endsection