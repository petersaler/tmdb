require('./bootstrap');
window.Vue = require('vue');
Vue.use(require('vue-resource'));
Vue.component('movie-component', require('./components/MovieComponent').default);
Vue.component('pagination', require('laravel-vue-pagination'));
const app = new Vue({
    el: '#app'
});
